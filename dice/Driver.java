/**
 * Show how to use the Cube class we wrote.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

import java.util.Scanner;

public class Driver
{
  public static void main(String [] args)
  {
    // To use a class (that doesn't contain main() you need to declare
    // a variable of that type.
    Cube one, two;

    // The variable you declare isn't actually a Cube ... it is a
    // "reference" to a die (draw a picture) ... to create an actual
    // Cube you use the new operator.  This creates a new die and the
    // variable one now refers to (or points to) that new variable.
    one= new Cube();

    // To roll the die is easy:
    one.roll();

    // If I print the variable it will automatically invoke the toString
	 // method.
    System.out.println("Cube one: "+one);

    // I can't directly look at one's "currentValue" attribute because it
    // is declared to be private ...
    //System.out.println("Cube one: "+one.value);

    // So to see it's current value I call the getRoll method ...
    System.out.println("Cube one: "+one.getValue());


	 // What will it do?
    System.out.println();

    //System.out.println(two);
    two= new Cube();
    System.out.println(two);
	 two.roll();
    System.out.println(two);


    // represent 6 dice and roll them
  }

}
