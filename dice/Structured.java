/**
 * Roll some dice in the structure programming sort of way.
 *
 * @author	Terry Sergeant
 * @version for Program Design 2
 *
*/

import java.util.Scanner;

public class Structured
{
	public static void main(String [] args)
	{
		int one,two;

		one= roll();
		System.out.println("Die one: "+one);

		System.out.println();

		//System.out.println(two);
		two= roll();
		System.out.println(two);


		// represent 6 dice and roll them
	}

	public static int roll()
	{
		return 1+(int)(Math.random()*6.0);
	}
}

