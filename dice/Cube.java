/**
 * A simple class to impelment a 6-sided die.
 *
 * @author	 T.Sergeant
 * @version for Program Design 2
 */
class Cube
{
	/**
	 * The current current value of the die.
	 */
	private int value;


	/**
	 * When we create a new die we invoke the roll() method to make sure
	 * it contains a valid value.
	 */
	public Cube()
	{
		roll();
	}


	/**
	 * Show the value of the die if someone prints this object.
	 */
	public String toString()
	{
		return ""+value;
	}


	/**
	 * Roll the die using Math.random().
	 */
	public void roll()
	{
		value= 1+(int)(Math.random()*6.0);
	}


	/**
	 * Provides the current value of the die as an int.
	 * @return the current value of the die
	 */
	public int getValue()
	{
		return value;
	}
}
