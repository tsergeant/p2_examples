/**
 * Demonstrates how to write values to a text file.
 * 
 * @author Terry Sergeant
 * @version In Class Demo
 *
*/

import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class WriteFileDemo
{
	public static void main(String [] args) throws IOException
	{
		PrintStream f= new PrintStream(new File("junk.txt"));
		int x= 7;
		double y= 3.8234;

		f.print("This is so fun!\n\n");
		f.print("How are you?\n");
		f.println("What do we do now?");
		f.printf("I am %6d years old and am %7.1f feet tall\n",x,y);
		f.close();
	}
}

