/**
 * Displays the contents of a file named "fun.txt" to the screen.
 *
 * @author Terry Sergeant
 * @version In Class Demo
 *
 * Perform the following steps:
 *
 * 1. Copy this program to your own working space.  Then read the source
 *    code and predict the behavior.  Then compile it and run it.  Your
 *    probably received a run-time error ... why is that?
 * 2. Use the jGrasp text editor to create a file named 'fun.txt' and put
 *    it in the same folder as this file.  Now run the program and see if
 *    you get the expected behavior.
 * 3. Modify this program so that instead of always opening a file named
 *    'fun.txt' it will open a file name specified by the user.  Test the
 *    program will several files.  What happens if you enter the file name
 *    ShowFile.java ?
 * 4. Use the jGrasp text editor to create a file named 'numbers.txt' that
 *    contains a list of integers (one per line).  Will this program still
 *    work when it reads a file containing only numbers?  Try it.
 * 5. Modify this program so that it uses .nextInt() to read from the
 *    file instead of using .nextLine() ... you may want to set the file
 *    name to numbers.txt for the next set of steps so you won't have to
 *    re-enter it every time you run the program.  Make sure the program
 *    still works on 'numbers.txt'
 * 6. Modify the program so that it will read all the numbers into an array
 *    of ints.  Then, in a separate loop, after the file has already been
 *    closed, display all of the numbers that were obtained from the file.
 * 7. Modify the loop that displays all of the numbers to instead find the
 *    largest value in the array.  Display only the largest value (after
 *    the loop).
*/

import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class ShowFile
{
	public static void main(String [] args) throws IOException
	{
		Scanner demo;
		String str;
		int    lines;

		demo= new Scanner(new File("fun.txt"));
		lines= 0;
		while (demo.hasNextLine()) 
		{
			str= demo.nextLine();
			System.out.println(str);
			lines++;
		}

		System.out.println("The file 'fun.txt' contained "+lines+" lines.");
		demo.close();

	}
}
