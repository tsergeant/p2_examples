/**
 * Demos a method that will find an element in an array of ints.
 *
 * @author  Terry Sergeant
 * @date    29 Oct 2007
 *
 * Write a method called findPosition that accepts three parameters:
 *   - an array of ints
 *   - an int that represents the number of elements in the array
 *   - an int that represents a value to be searched for in the array
 * The method should return the index at which the search element is
 * found; -1 if not found.
 *
*/

import java.util.Scanner;

public class SearchSoln
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		int [] ids;
		int n;
		int i;
		int num= 0;


		ids= new int[100];
		n= 20;

		for (i=0; i<n; i++)
			ids[i]= (int) (Math.random()*200.0);

		for (i=0; i<n; i++)
			System.out.println(ids[i]);
	 
		i= findPosition(ids,n,50);
		if (i==-1)
			System.out.println("I'm sorry, but 50 is not in the list.");
		else
			System.out.println("The value 50 was found at position "+i);

		System.out.print("Enter a value to search for: ");
		num= kb.nextInt();

		i= findPosition(ids,n,num);
		if (i==-1)
			System.out.println("I'm sorry, but "+num+" is not in the list.");
		else
			System.out.println("The value "+num+" was found at position "+i);
	}

	/**
	 * Searches for searchVal in theArray having arraySize elements.
	 *
	 * @param theArray array to be searched
	 * @param arraySize number of elements in the array
	 * @param searchVal the value for which we are searching the array
	 * @return position at which searchVal is found in theArray (-1 if not * found)
	*/
	public static int findPosition(int [] theArray, int arraySize, int searchVal)
	{
		int i;

		for (i=0; i<arraySize; i++)
			if (theArray[i]==searchVal)
				return i;
		return -1;
	}
}

