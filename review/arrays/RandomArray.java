/**
 * Demonstrates use of arrays.
 *
 * @author  Terry Sergeant
 * @version 29 Oct 2008
 *
*/

import java.util.Scanner;

public class RandomArray
{
	public static void main(String [] args)
	{
		Scanner cin= new Scanner(System.in);
		int i,n;
		double [] numbers= new double [100];
		double sum,avg;
		int maxPos,minPos, pos;

		System.out.print("How many numbers would you like to work with: ");
		n= cin.nextInt();

		for (i=0; i<n; i++)
			numbers[i]= Math.random();

		/*
		for (i=0; i<3; i++) {
			System.out.print("Enter a value: ");
			numbers[n]= cin.nextDouble();
			n++;
		}
	 */

		System.out.println("Here are your numbers!");
		for (i=0; i<n; i++)
			System.out.println(numbers[i]);

		sum= 0.0;
		for (i=0; i<n; i++)
			sum+= numbers[i];
		avg= sum / n;
		System.out.println("Sum: "+sum+", Avg: "+avg);

		maxPos= 0;
		minPos= 0;

		for (i=1; i<n; i++)
			if (numbers[i] > numbers[maxPos])
				maxPos= i;
			else if (numbers[i] < numbers[minPos])
				minPos= i;

		System.out.println("Max value is: "+numbers[maxPos]);
		System.out.println("Min value is: "+numbers[minPos]);

		// let's be nice to ourselved and assume that max and min are different
		if (maxPos < minPos)  // remove minPos first
		{
			for (i=minPos; i<n-1; i++)
				numbers[i]= numbers[i+1];
			n--;
			for (i=maxPos; i<n-1; i++)
				numbers[i]= numbers[i+1];
			n--;
		}
		else 
		{
			for (i=maxPos; i<n-1; i++)
				numbers[i]= numbers[i+1];
			n--;
			for (i=minPos; i<n-1; i++)
				numbers[i]= numbers[i+1];
			n--;
		}

		System.out.println("Here they are w/o max and min!");
		for (i=0; i<n; i++)
			System.out.printf("[%2d] %6.3f\n",i,numbers[i]);

		System.out.print("Which element would you like to remove (-1 to quit): ");
		pos= cin.nextInt();
		while (pos >= 0) 
		{
			for (i=pos; i<n-1; i++)
				numbers[i]= numbers[i+1];
			n--;

			System.out.println("Here they are w/o the number you said!");
			for (i=0; i<n; i++)
				System.out.printf("[%2d] %6.3f\n",i,numbers[i]);

			System.out.print("Which element would you like to remove (-1 to quit): ");
			pos= cin.nextInt();
		}

	}
}

