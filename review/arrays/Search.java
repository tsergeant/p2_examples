/**
 * Demos a method that will find an element in an array of ints.
 *
 * @author  Terry Sergeant
 * @date    29 Oct 2007
 *
 * Write a method called findPosition that accepts three parameters:
 *   - an array of ints
 *   - an int that represents the number of elements in the array
 *   - an int that represents a value to be searched for in the array
 * The method should return the index at which the search element is
 * found; -1 if not found.
 *
*/

import java.util.Scanner;

public class Search
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		int [] ids;
		int i,n;
		int pos;
		int num;

		ids= new int[100];
		n= 20;

		// put 20 random values into the array
		for (i=0; i<n; i++)
			ids[i]= (int) (Math.random()*200.0);

		// print the 20 random values 
		for (i=0; i<n; i++)
			System.out.println(ids[i]);

		System.out.print("Enter value to search for: ");
		num= kb.nextInt();

		pos= findPosition(ids,n,num);
		if (pos==-1)
			System.out.println(num+" not found!");
		else
			System.out.println("Found "+num+" at pos "+pos);
	}

	/**
	 * Searches for searchVal in theArray having arraySize elements.
	 *
	 * @param theArray array to be searched
	 * @param arraySize number of elements in the array
	 * @param searchVal the value for which we are searching the array
	 * @return position at which searchVal is found in theArray (-1 if not * found)
	*/
	public static int findPosition(int [] theArray, int arraySize, int searchVal)
	{
		int i;
		for (i=0; i<arraySize; i++)
			if (theArray[i]==searchVal)
				return i;
		return -1;
	}
}

