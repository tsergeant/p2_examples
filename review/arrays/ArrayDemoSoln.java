/**
 * Demonstrates use of arrays; solution to ArrayDemo.java.
 *
 * @author  Terry Sergeant
 * @date    09 Nov 2006
 *
 * Do the following steps:
 * 1. Read the source code and see if you understand what it will do.
 * 2. Run the program and observe its behavior.
 * 3. Set a breakpoint at the line labeled "A" and then trace the
 *    execution of the program making one step at a time.  Notice
 *    how the value of i changes and how the array fills up.
 * 4. Replace the while loops with for loops that do the same thing.
 *    Test your program to make sure it still works correctly.
 * 5. It is possible to reserve the exact amount of memory you need
 *    for an array (if you know how much you need).  Modify the program
 *    so that it reserves "n" integers for the array instead of 100
 *    integers.  Make sure the variable "n" has a value before you
 *    use it to reserve memory.
 * 6. In the for loops replace the variable "n" with "numbers.length"
 *    What do you think would happen if you made this replacement
 *    *before* you changed how much memory you reserved?
 * 7. Down below the line labeled "B" add a loop that will sum the 
 *    values of the array.  After the new loop calculate the average
 *    value.  Test your program with these numbers: n=3, numbers= 1,2,4
 *    Does your program give the correct answer?
 * 8. Down below the line labeled "C" add a loop that will double 
 *    every element in the array.  Then, in a separate loop, display
 *    the contents of the array.
*/

import java.util.Scanner;

public class ArrayDemoSoln
{
	public static void main(String [] args)
	{
		Scanner cin= new Scanner(System.in);
		int i,n,sum;
		int [] numbers;
		double avg;

		System.out.print("How many numbers would you like to enter: ");
		n= cin.nextInt();
		numbers= new int[n];

		/* A */
		for (i=0; i<numbers.length; i++) {
			System.out.print("Enter number "+i+": ");
			numbers[i]= cin.nextInt();
		}

		System.out.println("Here are your numbers!");
		for (i=0; i<numbers.length; i++)
			System.out.println(numbers[i]);

		/* B */
		sum= 0;
		for (i=0; i<numbers.length; i++)
			sum+= numbers[i];
		avg= (double) sum / numbers.length;
		System.out.println("Average: "+avg);

		/* C */
		for (i=0; i<numbers.length; i++)
			numbers[i]*= 2;
		System.out.println("Here are your numbers doubled!");
		for (i=0; i<numbers.length; i++)
			System.out.println(numbers[i]);


	}
}

