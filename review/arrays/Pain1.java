/**
 * Demos how tough life is without arrays.
 *
 * @author  Terry Sergeant
 * @date    19 Oct 2007
 *
 * 1. Modify this program so that it allows the user to enter 10 numbers
 *    and AFTER all 10 numbers have been entered, it displays the 10
 *    numbers to the screen.  (Don't move to the second step until the 
 *    first is working.)  HINT: You may need to do away with the loop.
 * 2. Now modify the program so that you ask the user how many numbers
 *    they want to input and then allow them to input that number.  AFTER
 *    all those numbers are entered, you will display them.
 *
*/

import java.util.Scanner;

public class Pain1
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		int num0;
		int num1;
		int num2;
		int num3;
		int num4;
		int num5;
		int num6;
		int num7;
		int num8;
		int num9;

		System.out.print("Enter a number: ");
		num0= kb.nextInt();
		System.out.print("Enter a number: ");
		num1= kb.nextInt();
		System.out.print("Enter a number: ");
		num2= kb.nextInt();
		System.out.print("Enter a number: ");
		num3= kb.nextInt();
		System.out.print("Enter a number: ");
		num4= kb.nextInt();
		System.out.print("Enter a number: ");
		num5= kb.nextInt();
		System.out.print("Enter a number: ");
		num6= kb.nextInt();
		System.out.print("Enter a number: ");
		num7= kb.nextInt();
		System.out.print("Enter a number: ");
		num8= kb.nextInt();
		System.out.print("Enter a number: ");
		num9= kb.nextInt();

		System.out.println("Here are your numbers: ");
		System.out.println(num0);
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);
		System.out.println(num5);
		System.out.println(num6);
		System.out.println(num7);
		System.out.println(num8);
		System.out.println(num9);
	}

}

