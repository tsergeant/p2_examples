/**
 * Demos some things with strings.
 *
 * @author Terry Sergeant
 * @version In Class Demo
 *
 * 1. Read the code and predict.
 * 2. Run it using your name.
 * 3. Run it with the name "Mary"
 * 4. Run it with the name "james" (with a lowercase j)
 * 5. Visit the website given and looks at the list of methods!
*/

import java.util.Scanner;

public class String3
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		String first;
		String sentence;
		int pos;

		System.out.print("Enter your name: ");
		first= kb.nextLine();
		System.out.println("Your name has "+first.length()+" characters.");
		System.out.println("Here it is in all caps     : "+first.toUpperCase());
		System.out.println("Here it is in all lowercase: "+first.toLowerCase());

		if (first=="Mary")
			System.out.println("Congratulations!  You have the most common girls name.");
		if (first.equals("James"))
			System.out.println("Congratulations!  You have the most common boys name.");

		System.out.println("Here is your name displayed vertically:");
		pos= 0;
		while (pos < first.length()) {
			System.out.println(first.charAt(pos));
			pos= pos + 1;
		}
		// How could you modify this so that it prints diagonally down?


		// NOTE: For a complete list of "methods" available for string
		// variables see:
		// http://download.oracle.com/javase/6/docs/api/java/lang/String.html
	}

}

