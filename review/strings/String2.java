/**
 * Demos some things with strings.
 *
 * @author Terry Sergeant
 * @version In Class Demo
 *
 * 1. Read the source code and predict the results.
 * 2. Run the program and follow the instructions.  Compare the results
 *    with your predictions.  Try to explain the results.
 * 3. Try to "fix" the program so it work as desired.
 *
*/

import java.util.Scanner;

public class String2
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		String first;
		String sentence;
		int num;
	 
	 // System.out.print("Enter your first name: ");
		//first= kb.nextLine();
	 System.out.print("Enter a number: ");
	 num= kb.nextInt();

		System.out.print("Enter a short sentence: ");
		kb.nextLine();
		sentence= kb.nextLine();

		//System.out.println("Your first name: "+first);
	 System.out.println(num);
		System.out.println("Your sentence  : "+sentence);
	}

}

