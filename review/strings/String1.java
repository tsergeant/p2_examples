/**
 * Demos some things with strings.
 *
 * @author Terry Sergeant
 * @version In Class Demo
 *
 * 1. Read the source code and predict the results.
 * 2. Run the program and follow the instructions.  Compare the results
 *    with your predictions.
 * 3. Run the program again, but this time enter your first AND last name
 *    when prompted to enter your first name.  Try to explain the results!
 *
*/

import java.util.Scanner;

public class String1
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		String first,last;
		String sentence;
		String full;

		System.out.print("Enter a short sentence: ");
		sentence= kb.nextLine();
		System.out.print("Enter your first name: ");
		first= kb.next();
		System.out.print("Enter your last name: ");
		last= kb.next();

		System.out.println("Your first name: "+first);
		System.out.println("Your last name : "+last);
		System.out.println("Your sentence  : "+sentence);

		full= first + " " + last;
		System.out.println("Your full name : "+full); 

		full= "Your full name : " + full;
		System.out.println(full);   // do you see why we use + now?

	}

}

