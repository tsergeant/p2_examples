/**
 * Encourages investigation of String, StringBuffer, and array of char.
 * @author  Terry Sergeant
 * @date    29 Nov 2006
 *
 * Read the code and comments below.  Take your time because there is 
 * a lot of information.  Every so often pause to run the program and 
 * then observe the behavior of program for the statements you've just
 * learned about.
 *
 * After you've read and understood the examples, take a minute to look
 * at: http://java.sun.com/j2se/1.3/docs/api/java/lang/String.html
 * This page contains the official documentation for the "String class".
 * Move down to the section entitled "Method Summary".  This table lists
 * all of the methods (functions) that exist for a variable of type
 * String.  Browse through the list and look for methods that you have
 * used in the past and ones you have just learned about.
 *
 * Can you figure out the difference between methods that have the
 * keyword static in front of them and those that do not?  Try to figure
 * it out and then write a short program that demonstrates your
 * conclusions.
 *
 * Similar documentation exists for StringBuffer.
 *

*/

import java.util.Scanner;

public class StringBufferDemo
{
	public static void main(String [] args)
	{
		Scanner cin= new Scanner(System.in);
		int i;
		
		// A list of characters can be stored as String or as an array of char
		String s1, s2;
		char [] a1,a2;

		// There are advantages to using each.  For example, strings make it
		// easy to assign and display words ...
		s1= "strings are fun";
		System.out.println("String: "+s1);
		System.out.print("Enter your name: ");
		s2= cin.nextLine();
		System.out.println("Your name is: "+s2);

		// to do the same thing with an array would be tedious ...
		a1= new char[14];
		a1[0]= 'a';
		a1[1]= 'r';
		a1[2]= 'r';
		a1[3]= 'a';
		a1[4]= 'y';
		a1[5]= 's';
		a1[6]= ' ';
		a1[7]= 'a';
		a1[8]= 'r';
		a1[9]= 'e';
		a1[10]= ' ';
		a1[11]= 'f';
		a1[12]= 'u';
		a1[13]= 'n';
	 
		System.out.println("Array: "+a1); // make note of what get's displayed here!
		// Let's try this instead!
		System.out.print("Array: ");
		for (i=0; i<a1.length; i++)
			System.out.print(a1[i]);
		System.out.println();

		// an alternative is to use a static method to convert the array to
		// a String and then display it:
		System.out.println("Array (again): "+String.valueOf(a1));

		System.out.print("Enter your dog's name: ");
		a2= cin.nextLine().toCharArray();
		// NOTE: cin.nextLine() returns a String ... and there is a method
		// called .toCharArray() that will convert the string in question to
		// an array of chars.
		// NOTE 2: Because it's such a pain to show the contents of the 
		// array, it can be useful to write a function to display it ...
		System.out.print("Dog's name: ");
		showArray(a2);  // check this function out


		// Incidently, you can view the individual letters of a String by using
		// the .charAt(pos) method:
		System.out.print("String (one char at a time): ");
		for (i=0; i<s1.length(); i++)
			System.out.print(s1.charAt(i));
		System.out.println();

		//----------------------------------------------------------------------------
		// Arrays *do* have some advantages, though!
		// For example, suppose we want to replace all 'a' with hypens ...
		for (i=0; i<a1.length; i++)
			if (a1[i]=='a')
				a1[i]= '-';
		System.out.print("Modified array: ");
		showArray(a1);


		// Doing the same thing (replacing all 'a' chars with hypens with a
		// string is bothersome and requires the use of an inefficient function
		// called substring:
		for (i=0; i<s1.length(); i++)
			if (s1.charAt(i)=='a')
				s1= s1.substring(0,i)+"-"+s1.substring(i+1,s1.length());
		System.out.println("Modified String: "+s1);

		//--------------------------------------------------------------------
		// If you haven't done so already, take time to compile and run this
		// program and see if the output matches what you expected.
		//--------------------------------------------------------------------


		//--------------------------------------------------------------------
		// Wouldn't it be nice to have the advantages of a String (easy to
		// set, read, and display) with the advantages of an array (easy to
		// modify)?
		//
		// INTRODUCING: StringBuffer!
		//
		StringBuffer sb1, sb2;

		// To assign a value you must use "new" as follows:
		sb1= new StringBuffer("string buffers rock");
		System.out.println("StringBuffer: "+sb1);

		// To get a value from a user you do the same thing ... except use
		// the scanner class to obtain the value:
		System.out.print("What did you say your name was again? ");
		sb2= new StringBuffer(cin.nextLine());
		System.out.println("Oh, yeah ... "+sb2);

		// When comparing StringBuffer to a string you won't have much
		// luck:
		s1= "fun";
		sb1= new StringBuffer("fun");

		/*
		if (sb1==s1)   // won't compile b/c types don't match
			System.out.println("They match!");
		else
			System.out.println("They dont't match!");
		*/

		// oh, yeah ... but what about .equals()?
		if (sb1.equals(s1))
			System.out.println("They match!");
		else
			System.out.println("They dont't match!");

		// it turns out that this will compile, but won't compare the strings
		// the way we want ... so, we must convert the StringBuffer to a
		// String (using .toString()) and then use the .equals() method:
		if (sb1.toString().equals(s1))
			System.out.println("They match!");
		else
			System.out.println("They dont't match!");


		//----------------------------------------------------------------------------
		// Now, how about the issue of modifying the characters in the string?
		// Let's replace all the r's with t's ...
		sb1= new StringBuffer("string buffers rock");
		System.out.println("Before: "+sb1);
		for (i=0; i<sb1.length(); i++)
			if (sb1.charAt(i)=='r')
				sb1.setCharAt(i,'t');
		System.out.println("After : "+sb1);
		
	}

	/**
	 * Displays the array are chars as a word.
	 * @param word The array to be displayed
	*/
	public static void showArray(char [] word)
	{
		int i;
		for (i=0; i<word.length; i++)
			System.out.print(word[i]);
		System.out.println();
	}
}

