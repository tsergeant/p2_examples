/**
 * Demonstrates return by value, scope of local variables, etc,
 * (in the context of static (non-OOP) functions).
 *
 * @author Terry Sergeant
 * @version In Class Demo
*/
import java.util.Scanner;

public class FuncDemo5
{
	public static void main(String [] args)
	{
		int x,y,z;
		x= 1;
		y= 2;
		z= 3;
		System.out.println("Values of x, y, and z (at start): "+x+", "+y+", "+z);
		func1(x);

		/*
		System.out.println("Values of x, y, and z (after func1): "+x+", "+y+", "+z);
		func2(x);
		System.out.println("Values of x, y, and z (after func2): "+x+", "+y+", "+z);
		z= func3(x,y);
		System.out.println("Values of x, y, and z (after func3): "+x+", "+y+", "+z);
		System.out.println("--------------------------------------------------");

		int [] d,e;
		d= new int[3];
		d[0]= 1; d[1]= 2; d[2]= 3;
		System.out.println("values in d (at start): "+d[0]+", "+d[1]+", "+d[2]);
		func4(d);
		System.out.println("values in d (after func4): "+d[0]+", "+d[1]+", "+d[2]);
		func5(d);
		System.out.println("values in d (after func5): "+d[0]+", "+d[1]+", "+d[2]);
		e= func6(d);
		System.out.println("values in e (after func6): "+e[0]+", "+e[1]+", "+e[2]);
		*/
	}

	/*
	** Lessons: parameters are passed by value and local variables
	**   are distinct from those declared in main.
	**------------------------------------------------*/
	public static void func1(int a)
	{
		int z;  // this local variable has the same name as a variable in main 
		z= 10;
		a= 20;
		System.out.println("in func1 a and z are: "+a+", "+z);
	}
	public static void func2(int x)
	{
		x= 10;
		System.out.println("in func2 x is: "+x);
	}
	public static int func3(int a, int b)
	{
		a= 100*a;
		b= 20*b;
		System.out.println("in func1 a and b are: "+a+", "+b);
		return a+b;
	}
	public static void func4(int [] x)
	{
		x[0]= 10;
		x[1]= 20;
		x[2]= 30;
		System.out.println("in func2 x holds: "+x[0]+", "+x[1]+", "+x[2]);
	}

	public static void func5(int [] x)
	{
		x= new int[3];
		x[0]= 100;
		x[1]= 200;
		x[2]= 300;
		System.out.println("in func2 x holds: "+x[0]+", "+x[1]+", "+x[2]);
	}

	public static int [] func6(int [] x)
	{
		int [] a= new int[3];
		a[0]= x[0];
		a[1]= x[1];
		a[2]= x[2];
		System.out.println("in func2 x holds: "+a[0]+", "+a[1]+", "+a[2]);
		return a;
	}
}
