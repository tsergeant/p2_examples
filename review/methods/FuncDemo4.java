/**
 * Demonstrates a static function with parameters that returns a non-numeric value.
 *
 * @author Terry Sergeant
 * @version In Class Demo
*/
import java.util.Scanner;

public class FuncDemo4
{
	public static void main(String [] args)
	{
		int grade;
		Scanner scan= new Scanner(System.in);

		System.out.print("Enter score (0-100): ");
		grade= scan.nextInt();
		System.out.println("Your letter grade is: " + letterGrade(grade));
	}

	/*
	** given score returns letter grade
	**------------------------------------------------*/
	public static char letterGrade(int score)
	{
		switch (score / 10) {
			case 10:
			case 9 : return 'A';  // why no break statement here?
			case 8 : return 'B';
			case 7 : return 'C';
			case 6 : return 'D';
			default: return 'F';
		}
	}
}
