/**
 * Demonstrates a static function with no parameters.
 *
 * @author Terry Sergeant
 * @version In Class Demo
*/

public class FuncDemo1
{
	public static void main(String [] args)
	{
		System.out.print("Dashing through the snow\n");
		System.out.print("In a one-horse, open sleigh\n");
		System.out.print("O'er the fields we go\n");
		System.out.print("Laughing all the way\n");
		System.out.print("And some more words ...\n");
		refrain();
		System.out.print("The words to the second verse go here.\n");
		System.out.print("I should have used a song I know the\n");
		System.out.print("word to.  At any rate, after the second verse\n");
		System.out.print("we want to sing the refrain again: \n");
		refrain();
	}

	// prints words to refrain of "Jingle Bells"
	public static void refrain() 
	{
		System.out.print("\n");
		System.out.print("Jingle bells, Jingle bells\n");
		System.out.print("Jingle all the way\n");
		System.out.print("Oh, what fun it is to ride\n");
		System.out.print("In a one-horse, open sleigh\n");
		System.out.print("\n");
	}
}
