/**
 * Demonstrates a static function with parameters
 *
 * @author Terry Sergeant
 * @version In Class Demo
*/
import java.util.Scanner;

public class FuncDemo2
{
	public static void main(String [] args)
	{
		Scanner scan;
		int idnumber, age;
		String firstname;

		scan= new Scanner(System.in);

		printInfo(1234,"Henry",40);
		printInfo(2345,"Yolanda",25);

		System.out.print("Enter your id number: ");
		idnumber= scan.nextInt();
		System.out.print("Enter your age      : ");
		age= scan.nextInt();
		System.out.print("Enter your name     : ");
		firstname= scan.next();

		printInfo(idnumber,firstname,age);
	}

	// prints id, name, and age "real pretty like"
	public static void printInfo(int id, String name, int age)
	{
		System.out.print("--------------------\n");
		System.out.println("Info for ID # " + id);
		System.out.print("--------------------\n");
		System.out.println("Name: " + name);
		System.out.println("Age : " + age);
		System.out.print("--------------------\n");
	}
}
