/**
 * Demonstrates a static function with parameters that returns a value.
 *
 * @author Terry Sergeant
 * @version In Class Demo
*/
import java.util.Scanner;

public class FuncDemo3
{
	public static void main(String [] args)
	{
		Scanner scan;
		double x,y,z;
		double ans1, ans2;

		scan= new Scanner(System.in);

		System.out.print("Enter coefficients: ");
		x= scan.nextDouble();
		y= scan.nextDouble();
		z= scan.nextDouble();

		ans1= root1(x,y,z);
		ans2= root2(x,y,z);
		System.out.println("Roots: " + ans1  +" ,"  +ans2);

		System.out.println("--------------------------------------");
		System.out.println("Root1: " + root1(1.0,5.2,2.5));
		System.out.println("Root2: " + root2(1.0,5.2,2.5));
	}

	/*
	** calculates 1st/2nd roots of quadratic equation
	**------------------------------------------------*/
	public static double root1(double a, double b, double c)
	{
		double d;
		d= (-b+Math.sqrt(b*b-4*a*c)) / (2*a);
		return d;
	}

	public static double root2(double a, double b, double c)
	{
		return (-b-Math.sqrt(b*b-4*a*c)) / (2*a);
	}
}
