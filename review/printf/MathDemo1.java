/**
 * Demonstrates short-cut operators, Math library functions, and mixing types.
 *
 * @author Terry Sergeant
 * @version In Class Demo
 *
*/

import java.util.Scanner;

public class MathDemo1
{
	public static void main(String [] args)
	{
		int    i,j,k;
		float  a,b,c;
		double x,y,z;

		// Lesson -> i=i+1  is the same as i++
		i= 0;
		i= i + 1;
		System.out.println("i: "+i);
		i++;
		System.out.println("i: "+i);

		// Lesson -> i=i+j  is the same as i+= j
		// NOTE: This work for -, *, and / too
		j= 50;
		i= i + j;
		System.out.println("i: "+i);
		i+= j;
		System.out.println("i: "+i);

		// Lesson -> the Math libary has lots of functions
		x= 0.0;
		y= 25.0;
		z= 5.59;
		System.out.println("sqrt of "+y+" is "+Math.sqrt(y));
		System.out.println(z+" to the 3rd power is "+Math.pow(z,3.0));
		System.out.println("Here's a random value (0.0 to 0.999999): "+Math.random());
		System.out.println("ln "+y+" is "+Math.log(y));
		System.out.println("sin "+x+" is "+Math.sin(x));
		
		// Lesson -> Functions and operators can be combined to produce
		//   more complicated expressions.
		x= 1.0 / ((Math.PI * Math.sqrt(z) - Math.pow(z,2.2)) / (y-Math.atan(x)));
		System.out.println("x: "+x);

		// Lesson -> Different numeric types can be mixed; the result
		//   is the more general type.  Also, constants are "double" values
		//   unless you use "f" to make them floats
		x= 0.0;
		a= 1.2f;
		b= 2.0f;
		c= 33.823f;
		System.out.println("Mixed expression: "+(x*c+z*b-i));

		// Lesson -> When storing a "general" expression into a less 
		//   general type you must type cast ... and possibly lose info.
		i= (int) a;
		System.out.println("i: "+i);
		i= (int) c;
		System.out.println("i: "+i);
		b= (float) z;
		System.out.println("b: "+b);
	}
}
