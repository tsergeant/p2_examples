/**
 * Demonstrates limitations of float and double types.
 *
 * @author Terry Sergeant
 * @version In Class Demo
 *
 * Description: Calculates a function that uses floating point
 *   arithmetic.  The same function is calculated twice with the
 *   only difference being the order of the loops.  The output
 *   will differ slightly in each case ...
 *
 *    The function being calculated is:
 *      \sum_{i=1}^n 100 / (i*i)
 *
 *  Illustrates: floating point errors and compares float and double;
 *    at the time of introduction, this program also illustrates the
 *    use of "for" loops and of shortcut operators.
 *
*/

public class MathDemo3
{
	public static void main(String [] args)
	{
		int    i;
		float  fsuma, fsumb;
		double dsuma, dsumb;

		/*------------------ Use float sums */
		fsuma= fsumb= 0.0f;
		for (i=1; i<=10000; i++) {   /* count up */
			fsuma+= 100.0 / (i * i);
		}
		for (i=10000; i>=1; i--) {   /* count down */
			fsumb+= 100.0 / (i * i);
		}

		System.out.printf("(float)  Counting UP  : %1.20f\n",fsuma);
		System.out.printf("(float)  Counting DOWN: %1.20f\n",fsumb);

		
		/*---------------------- Use double sums */
		dsuma= dsumb= 0.0;
		for (i=1; i<=10000; i++) {
			dsuma+= 100.0 / (i * i);
		}
		for (i=10000; i>=1; i--) {
			dsumb+= 100.0 / (i * i);
		}
		System.out.printf("(double)  Counting UP  : %1.20f\n",dsuma);
		System.out.printf("(double)  Counting DOWN: %1.20f\n",dsumb);

		System.out.printf("-------------------------------------------------\n");
		fsuma= 2.1f;
		System.out.printf("The number is: %1.20f\n",fsuma);
		dsuma= 2.2;
		System.out.printf("The number is: %1.50f\n",dsuma);
	}
}
