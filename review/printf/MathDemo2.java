/**
 * Demonstrates formatting of numeric output using printf.
 *
 * @author Terry Sergeant
 * @version In Class Demo
 *
*/

import java.util.Scanner;

public class MathDemo2
{
	public static void main(String [] args)
	{
		String myname= "Fred";
		int age= 13;
		double height= 4.87;
		double x,y,z;
		int    i,j,k;

		System.out.printf("My string: %10s\n","Fred");
		System.out.printf("My string: %10s\n","Freddy");
		System.out.printf("My string: %10s\n","Fredonski");
		System.out.printf("My string: %10s\n","Fre");
		System.out.printf("My string: %10s\n","F");
		System.out.printf("My string: %10s\n","Freddoniskylatrapinski");
		/*
		// Lesson -> intro to formatted output using printf
		//   %s is a placeholder for a string
		//   %d is a placeholder for an int
		//   %f is a placeholder for a float/double
		//   order matters!
		//   you use \n to move to next line on screen
		//   floats/doubles show 6 decimal places by default
		System.out.printf("Hi, my name is %s!  What's yours?\n",myname);
		System.out.printf("My name is %s! I am %d years old and I am %f feet tall.\n",myname,age,height);
		System.out.printf("I am %f feet tall. My name is %s. I am %d years old.\n",height,myname,age);
		//System.out.printf("Here are my two favorite numbers: %d and %f\n",1,2);
		//   type matters  this one produced a run-time error

		// Lesson -> field widths can be used to line up a column of numbers.
		x= 1.3; y= 127.5; z= 64.5;
		i= 56;  j= 1;     k= 822;

		System.out.printf("Here are some cool numbers:\n");
		System.out.printf("%5d %15f\n",i,x);
		System.out.printf("%5d %15f\n",j,y);
		System.out.printf("%5d %15f\n",k,z);
		System.out.printf("Here are again:\n");
		System.out.printf("%15d %25f\n",i,x);
		System.out.printf("%15d %25f\n",j,y);
		System.out.printf("%15d %25f\n",k,z);
		System.out.printf("One more time:\n");
		System.out.printf("%1d %1f\n",i,x);
		System.out.printf("%1d %1f\n",j,y);
		System.out.printf("%1d %1f\n",k,z);

		// Lesson -> You can show more or fewer decimal places

		System.out.printf("Here are some fun numbers:\n");
		System.out.printf("%15.3f\n",x);
		System.out.printf("%15.3f\n",y);
		System.out.printf("%15.3f\n",z);
		System.out.printf("Here they are again:\n");
		System.out.printf("%15.1f\n",x);
		System.out.printf("%15.1f\n",y);
		System.out.printf("%15.1f\n",z);
		System.out.printf("One more time:\n");
		System.out.printf("%15.11f\n",x);
		System.out.printf("%15.11f\n",y);
		System.out.printf("%15.11f\n",z);
		*/
	}
}
