/**
 * Demonstrates usage of two dimensional arrays in Java.
 *
 * @author	Terry Sergeant
 * @version for Program Design 2
 *
*/

import java.util.Scanner;

public class TwoD
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);

		int i,j;
		double [][] a;

		a= new double[3][4];

		for (i=0; i<3; i++)
			for (j=0; j<4; j++)
				a[i][j]= 1.0;

		display(a,3,4);
		System.out.println("------------------------");
		display(a);
		System.out.println("------------------------");


		for (i=0; i<a.length; i++)
			setRow(a[i],i);
		display(a);
		System.out.println("------------------------");


		//----------------------------------------------------------

		double [][] b;

		b= new double[10][];
		for (i=0; i<b.length; i++)
			b[i]= new double[i+1];

		for (i=0; i<b.length; i++)
			setRow(b[i],i);
		display(b);
		System.out.println("------------------------");


		//----------------------------------------------------------
		// Bonus 3d array!

		int k;
		double [][][] c;

		c= new double[5][4][6];
		for (i=0; i<c.length; i++)
			for (j=0; j<c[i].length; j++)
				for (k=0; k<c[i][j].length; k++)
					c[i][j][k]= 1.0;

		for (i=0; i<c.length; i++) {
			display(c[i]);
			System.out.println();
		}
		System.out.println("------------------------");

		for (i=0; i<c.length; i++)
			for (j=0; j<c[i].length; j++)
				setRow(c[i][j],i*2);

		for (i=0; i<c.length; i++) {
			display(c[i]);
			System.out.println();
		}
	}


	public static void display(double [][] a, int m, int n)
	{
		int i,j;

		for (i=0; i<m; i++)
		{
			for (j=0; j<n; j++)
				System.out.printf("%6.2f",a[i][j]);
			System.out.println();
		}
	}


	public static void display(double [][] a)
	{
		int i,j;

		for (i=0; i<a.length; i++)
		{
			for (j=0; j<a[i].length; j++)
				System.out.printf("%6.2f",a[i][j]);
			System.out.println();
		}
	}


	public static void setRow(double [] row, double val)
	{
		for (int i=0; i<row.length; i++)
			row[i]= val;
	}
}

