/**
 * An implementation of a StringContainer that provides the dictionary
 * operations for an unordered array of String values.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

public class StringContainer
{

	private String [] data; // data in the container
	private int       n;    // number of elements in the container

	/**
	 * Defines an empty StringContainer.
	 */
	public StringContainer() { clear(); }


	/**
	 * Defines an empty StringContainer.
	 *
	 * @param capacity the initial size of the container
	 */
	public StringContainer(int capacity)
	{
		n= 0;

		if (capacity>0 && capacity<1000000000)
			data= new String[capacity];
		else
			data= null;
	}


	/**
	 * Getter for the size of the container.
	 *
	 * @return size of the container
	 */
	public int size() { return n; }


	/**
	 * Empty out the container completely.
	 *
	 * @return size of the container
	 */
	public void clear()
	{
		data= null;
		n= 0;
	}


	/**
	 * Return the element at position index of the list.
	 *
	 * @param index a position in the list
	 * @return reference to the element at position index; null if index is
	 * not valid
	 */
	public String get(int index)
	{
		return null;
	}


	/**
	 * Insert elem into the array at the end.
	 *
	 * @param elem the string to be inserted at the end of the array
	 *
	 * <p>
	 * If the array happens to be full we don't want to fail. Instead we
	 * will double the size of our array and copy the elements from the
	 * original array into the new bigger array. Then we'll add the new
	 * element at the end of the new array.
	 * </p>
	 */
	public void add(String elem)
	{
	}


	/**
	 * Searches the container for elem and returns its location.
	 *
	 * @param elem the string to be found in the array
	 * @return the position in the list where the element was first found;
	 * returns -1 if not found
	 */
	public int indexOf(String elem)
	{
		return -1;
	}


	/**
	 * Removes first occurrence of elem from the list (if it is there).
	 *
	 * @param elem the string to be removed from the array
	 * @return true if elem was found; false otherwise
	 *
	 * <p>
	 * This operation is not required to maintain the order of the list.
	 * </p>
	 */
	public boolean remove(String elem)
	{
		return false;
	}


}

