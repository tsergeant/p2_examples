public class Driver
{
	public static void main(String [] args)
	{
		Student a= new Student(12345,"Fred",3.3);
		System.out.println(a);
		//System.out.println(a.id);
		System.out.println(a.gpa);
		System.out.println(a.name);
		System.out.println(a.favoriteColor);

		StudentAthlete b= new StudentAthlete(23456,"Mary",3.5,"Water Polo");
		System.out.println(b);
		//System.out.println(b.id);
		System.out.println(b.gpa);
		System.out.println(b.name);
		System.out.println(b.favoriteColor);
	}
}
