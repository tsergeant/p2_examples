/**
 * Everything you need to know about a student athlete.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

class StudentAthlete extends Student
{
	public String sport;


	public StudentAthlete(int newid, String newname, double newgpa, String sport)
	{
		super(newid,newname,newgpa);
		this.sport= sport;
	}

	public StudentAthlete()
	{
		super();
		sport="Curling";
	}

	public String toString()
	{
		//return "id: "+id+", name: "+name+", gpa: "+gpa+", color: "+favoriteColor+", sport: "+sport;
		return super.toString()+", sport: "+sport;
	}
}
