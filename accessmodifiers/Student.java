/**
 * Everything you need to know about a student.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

class Student
{
	// NOTE: There are four possible access modifiers in Java: public,
	// protected, no modifier, and private.

	// Consider these attributes:

	private int id;
	double gpa;
	protected String name;
	public String favoriteColor;


	public Student(int newid, String newname, double newgpa)
	{
		id= newid;
		name= newname;
		gpa= newgpa;
		favoriteColor= "green";
	}

	public Student()
	{
		id= 0;
		name= "Unknown";
		gpa= 0.0;
		favoriteColor= "green";
	}

	public String toString()
	{
		return "id: "+id+", name: "+name+", gpa: "+gpa+", color: "+favoriteColor;
	}
}
