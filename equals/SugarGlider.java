class SugarGlider {
	String name;
	double wingSpan;
	public SugarGlider(String name, double wingSpan) {
		this.name= name;
		this.wingSpan= wingSpan;
	}

	@Override
	public String toString() {
		return name+" ("+wingSpan+")";
	}

	@Override
	public boolean equals(Object that){
		if(this == that) return true;
		if(!(that instanceof SugarGlider)) return false;

		SugarGlider param= (SugarGlider)that;
		return this.name.equals(param.name);
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

}
