import java.util.LinkedList;
import java.util.Stack;
import java.util.Scanner;

public class Fun
{
	public static void main(String [] args)
	{
		SugarGlider a,b;

		a = new SugarGlider("Fred",12.1);
		b = new SugarGlider("Fred",12.1);
		System.out.println(a.toString());
		System.out.println(b.toString());
		System.out.println(a.hashCode());
		System.out.println(b.hashCode());
		if (a.equals(b)) {
			System.out.println("They are equal");
		}
		else {
			System.out.println("They are NOT equal");
		}
	}


	public static int fib(int n) {
		if (n < 2) return n;
		return fib(n-1) + fib(n-2);
	}

	public static void tower(String source, String dest, String temp, int n) {
		if (n == 1) {
			System.out.println("Move from "+source+" to "+dest);
		}
		else {
			tower(source,temp,dest,n-1);
			tower(source,dest,temp,1);
			tower(temp,dest,source,n-1);
		}
	}

}
