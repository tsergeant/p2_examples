/**
 * A base class that provides common code for testing various types of sorts.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 *
 * <p>To make use of this class, inherit from it and define sort().</p>
 */
import java.util.Scanner;
import java.io.File;

public abstract class SortTester
{
	protected Person [] data;
	protected int n;
	protected CodeTimer timer;

	/**
	 * Reserve memory for up to 2 million entries and load them from the file.
	 *
	 * @param filename name of file that holds contributors
	 */
	public SortTester(String filename)
	{
		timer= new CodeTimer();
		data= new Person[2000000];
		try {
			Scanner fin= new Scanner(new File(filename));
			n= 0;
			while (fin.hasNext()) {
				data[n++]= new Person(fin.next(),fin.next(),fin.nextInt());
				fin.nextLine();
			}
			fin.close();
		}
		catch (Exception e) {
			System.err.printf("ERROR: Unable to open file '%s'",filename);
		}
	}

	/**
	 * Display the first <code>howmany</code> entries (if they exist).
	 *
	 * @param howmany number of entries to display
	 */
	public void display(int howmany)
	{
		if (howmany > n)
			howmany= n;
		for (int i=0; i<howmany; i++)
			System.out.println(data[i]);
	}


	/**
	 * Display all entries.
	 */
	public void display()
	{
		display(n);
	}


	/**
	 * Sort the entries in accordance with compareTo().
	 */
	public abstract void sort();


	/**
	 * Sort elements and display top 20 results along with time elapsed.
	 */
	public void timedSort()
	{
		timer.start();
		sort();
		timer.stop();
		display(20);
		System.out.println("Time elapsed: "+timer);
	}
}
