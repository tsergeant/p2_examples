/**
 * Stores name an contribution amounts of individuals.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class Person
{
	private String firstname,lastname;
	private int contribution;

	public Person(String first, String last, int contrib)
	{
		firstname= first;
		lastname= last;
		contribution= contrib;
	}

	public String toString()
	{
		return String.format("%-15s %-15s %9d",firstname,lastname,contribution);
	}
}
