import java.util.Stack;

public class Tower
{
   public static void main(String [] args)
   {
      tower("L","R","M",3);
   }

   public static void tower(String from, String to, String using, int n)
   {
		Params p;
		Stack<Params> s= new Stack<Params>();
		s.push(new Params(from,to,using,n));
		while (!s.empty())
		{
			p= s.pop();
			if (p.n==1)
				System.out.println("Move from "+p.from+" to "+p.to);
			else {
				s.push(new Params(p.using,p.to,p.from,p.n-1));
				s.push(new Params(p.from,p.to,p.using,1));
				s.push(new Params(p.from,p.using,p.to,p.n-1));
			}
		}
   }
}

class Params
{
	String from, to, using;
	int n;
	public Params(String from, String to, String using, int n)
	{
		this.from= from;
		this.to= to;
		this.using= using;
		this.n= n;
	}
}
