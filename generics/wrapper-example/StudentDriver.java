/**
 * Demonstrates usage of MyStudentWrapper.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class StudentDriver
{
	public static void main(String[]args)
	{
		MyStudentWrapper w= new MyStudentWrapper(new Student("Joe",3.3));
		System.out.println(w);
		w.setValue(new Student("Alice",3.6));
		System.out.println(w.getValue());
	}
}
