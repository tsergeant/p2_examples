/**
 * Demonstrates usage of MyIntegerWrapper.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class IntegerDriver
{
	public static void main(String[]args)
	{
		MyIntegerWrapper w= new MyIntegerWrapper(20);
		System.out.println(w);
		w.setValue(30);
		System.out.println(w.getValue());
	}
}
