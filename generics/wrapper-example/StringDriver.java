/**
 * Demonstrates usage of MyStringWrapper.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class StringDriver
{
	public static void main(String[]args)
	{
		MyStringWrapper w= new MyStringWrapper("fun");
		System.out.println(w);
		w.setValue("cool");
		System.out.println(w.getValue());
	}
}
