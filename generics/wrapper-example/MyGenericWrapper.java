/**
 * Wraps an object in a class with appropriate getter/setter.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class MyGenericWrapper<T>
{
	private T value;
	public MyGenericWrapper(T initialVal) { value= initialVal; }
	public T getValue() { return value; }
	public void setValue(T newVal) { value= newVal; }
	@Override public String toString() { return ""+value; }
}
