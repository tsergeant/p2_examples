/**
 * Wraps student object in a class with appropriate getter/setter.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class MyStudentWrapper
{
	private Student value;
	public MyStudentWrapper(Student initialVal) { value= initialVal; }
	public Student getValue() { return value; }
	public void setValue(Student newVal) { value= newVal; }
	@Override public String toString() { return ""+value; }
}
