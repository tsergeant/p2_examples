/**
 * Demonstrates usage of MyGenericWrapper.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class GenericDriver
{
	public static void main(String[]args)
	{
		MyGenericWrapper<Student> stu= new MyGenericWrapper<Student>(new Student("Joe",3.3));
		System.out.println(stu);
		stu.setValue(new Student("Alice",3.6));
		System.out.println(stu.getValue());

		MyGenericWrapper<String> str= new MyGenericWrapper<String>("fun");
		System.out.println(str);
		str.setValue("cool");
		System.out.println(str.getValue());

		/*
		// NOTE: This won't work b/c we have to specify an object ... not a simple type
		MyGenericWrapper<int> i= new MyGenericWrapper<int>(20);
		System.out.println(i);
		i.setValue(30);
		System.out.println(i.getValue());
		*/

		MyGenericWrapper<Integer> i= new MyGenericWrapper<Integer>(20);
		System.out.println(i);
		i.setValue(30);
		System.out.println(i.getValue());


	}
}
