/**
 * Wraps an int value in a class with appropriate getter/setter.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class MyIntegerWrapper
{
	private int value;
	public MyIntegerWrapper(int initialVal) { value= initialVal; }
	public int getValue() { return value; }
	public void setValue(int newVal) { value= newVal; }
	@Override public String toString() { return ""+value; }
}
