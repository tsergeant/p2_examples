/**
 * A simple Student class.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class Student
{
   private String name;
   private double gpa;

	public Student(String name, double gpa) { this.name= name; this.gpa= gpa; }
	public String toString() { return String.format("%-20s %5.3f",name,gpa); }
}
