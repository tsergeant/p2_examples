/**
 * A simple ComparableStudent class.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */

//----------------------------- this is where I announce to the world I
//                              am defining how to compare myself to
//                              another object of the same type
//                              vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
public class ComparableStudent implements Comparable<ComparableStudent>
{
   private String name;
   private double gpa;

	public ComparableStudent(String name, double gpa) { this.name= name; this.gpa= gpa; }

	// NOTE: It is good form to annouce you are overriding a method using
	// the @Override directive in front of the method like this:
	@Override
	public String toString() { return String.format("%-20s %5.3f",name,gpa); }

	@Override
	public int compareTo(ComparableStudent other)
	{
		if (this.gpa > other.gpa) return +1;
		if (this.gpa < other.gpa) return -1;
		return 0;
	}

	// QUESTION: How would you rewrite this .compareTo method to be based on name
	// rather than gpa ... HINT: You can call String's own .compareTo method and
	// just return whatever it does!
}
