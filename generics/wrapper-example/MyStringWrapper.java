/**
 * Wraps a string value in a class with appropriate getter/setter.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 *
 * @see StringDriver
 *
 *
 * Question: What would need to change in this program to create a wrapper
 * for an int?
 *
 * @see MyIntegerWrapper
 * @see IntegerDriver
 *
 *
 * Question: What would need to change in this program to create a wrapper
 * for an Student?
 *
 * @see Student
 * @see MyStudentWrapper
 * @see StudentDriver
 */
public class MyStringWrapper
{
	private String value;
	public MyStringWrapper(String initialVal) { value= initialVal; }
	public String getValue() { return value; }
	public void setValue(String newVal) { value= newVal; }
	@Override public String toString() { return value; }
}
