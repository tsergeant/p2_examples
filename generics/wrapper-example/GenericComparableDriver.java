/**
 * Demonstrates usage of MyGenericComparableWrapper.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */
public class GenericComparableDriver
{
	public static void main(String[]args)
	{
		ComparableStudent a,b;
		MyGenericComparableWrapper<ComparableStudent> stu;
		a= new ComparableStudent("Joe",3.3);
		b= new ComparableStudent("Alice",3.6);

		stu= new MyGenericComparableWrapper<ComparableStudent>(a);
		System.out.println(stu);

		stu.brag(b);
		stu.setValue(b);

		System.out.println(stu.getValue());

		stu.brag(b);
		stu.brag(a);

		// NOTE: String and Integer classes implement Comparable ... so we can
		// store them in our wrapper as well!

		MyGenericComparableWrapper<String> str= new MyGenericComparableWrapper<String>("fun");
		System.out.println(str);
		str.setValue("cool");
		str.brag("hot");
		System.out.println(str.getValue());

		MyGenericComparableWrapper<Integer> i= new MyGenericComparableWrapper<Integer>(20);
		System.out.println(i);
		i.setValue(30);
		i.brag(0);
		System.out.println(i.getValue());
	}
}
