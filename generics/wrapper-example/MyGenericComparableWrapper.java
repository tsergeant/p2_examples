/**
 * Wraps an object in a class with appropriate getter/setter.
 *
 * @author  T.Sergeant
 * @version for Program Design 2
 */


//---------------------------------------- this is where we require T to
//                                         have implemented the Comparable
//                                         interface. In particular we
//                                         require they define how they will
//                                         be compared to other T objects
//                                     vvvvvvvvvvvvvvvvvvvvvvvvv
public class MyGenericComparableWrapper<T extends Comparable<T>>
{
	private T value;
	public MyGenericComparableWrapper(T initialVal) { value= initialVal; }
	public T getValue() { return value; }
	public void setValue(T newVal) { value= newVal; }

	// NOTE: this method can only work if objects of type T have a compareTo method!
	public void brag(T other) {
		int result= value.compareTo(other);
		if (result>0)
			System.out.println("I'm awesome");
		else if (result<0)
			System.out.println("You're awesome");
		else
			System.out.println("We're both mediocre");
	}

	@Override
	public String toString() { return ""+value; }
}


// NOTE: Normally we would want a compareTo method defined in a base class
// to be allowed to extend to an subclasses as well. To allow this we would
// write the header like this:
//public class MyGenericComparableWrapper<T extends Comparable<? super T>>
