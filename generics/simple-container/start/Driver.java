/**
 * Driver for testing MovieContainer class.
 *
 * @author  Terry Sergeant
 * @version for P2
 */

public class Driver
{
	public static void main(String [] args)
	{
		Movie one,two,three;

		// Lesson 1: every object has .hashCode() and .equals() because it
		// automatically inherits from Object. Read Javadocs.
		//
		// Lesson 2: if who objects are equal then their hashCodes need to
		// be equal.
		//
		// Lesson 3: the default behavior for .equals() is probably not what
		// we want (more on that later).

		one= new Movie("Up","Fun",1000);
		two= new Movie("Down","Fun",2000);
		three= new Movie("Up","Fun",1000);
		System.out.println("one's hashcode: "+one.hashCode());
		System.out.println("two's hashcode: "+two.hashCode());
		System.out.println("three's hashcode: "+three.hashCode());
		if (one.equals(two))
			System.out.println("one and two are equal");
		else
			System.out.println("one and two are not equal");

		if (one.equals(three))
			System.out.println("one and three are equal");
		else
			System.out.println("one and three are not equal");



		// Lesson 4: Behold the amazing MovieContainer
		MovieContainer mc= new MovieContainer();
		mc.insert(new Movie("Up","Fun",1000));
		mc.insert(new Movie("Down","Fun",2000));
		mc.display();

		// Lesson 5: MovieContainer is great for Movies but we want it
		// to be great for everything. Let's create an "AnythingContainer".
		//
		// Lesson 6: Why can't we seem to find the movie 'Up'?
		/*
		AnythingContainer<Movie> ac= new AnythingContainer<Movie>();
		ac.insert(new Movie("Up","Fun",1000));
		ac.insert(new Movie("Down","Fun",2000));
		ac.display();
		if (ac.indexOf(new Movie("Up","Fun",1000)) >=0)
			System.out.println("Found Movie Up");
		else
			System.out.println("Did not find 'Up'");
		*/

		// Lesson 7: But this works ... why?
		/*
		if (ac.indexOf(one)>=0)
			System.out.println("Found Movie Up");
		else
			System.out.println("Did not find 'Up'");

		ac.remove(one);
		ac.display();
		*/

		// Lesson 8: But what about strings?
		// Lesson 9: Somebody gave good and reasonable definitions to .equals
		// (and .hashCode) for String.
		/*
		String s1,s2,s3;
		s1= new String("hello");
		s2= new String("there");
		s3= new String("hello");
		System.out.println("s1's hashcode: "+s1.hashCode());
		System.out.println("s2's hashcode: "+s2.hashCode());
		System.out.println("s3's hashcode: "+s3.hashCode());
		if (one.equals(three))
			System.out.println("one and three are equal");
		else
			System.out.println("one and three are not equal");
		*/

		// Lesson 10: Let's override .equals() (and .hashCode())
		// (in Movie.java)


		// Lesson 11: Can we store other stuff in our AnythingContainer?

	}
}
