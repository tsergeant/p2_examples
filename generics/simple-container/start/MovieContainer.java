/**
 * A Movie container to the support the PersonalMovieManager.
 *
 * @author Terry Sergeant
 * @version for P2
 *
*/

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.PrintStream;

public class MovieContainer
{
	public static final int MAXBOOKS= 10000;
	private String dataFile;
	private Movie [] movies;
	private int numMovies;

	public MovieContainer()
	{
		movies= new Movie[100];
		numMovies= 0;
	}

	/**
	 * Displays all movie information.
	*/
	public void display()
	{
		int i;
		System.out.println("------------------------------------------------");
		System.out.printf("%6s %-30s %-20s %s\n","NUM","TITLE","GENRE","PAGES");
		for (i=0; i<numMovies; i++)
			System.out.printf("%6s %s\n",i,movies[i]);
	}


	/**
	 * Add new movie to container.
	 *
	 * @param title title of new movie to add
	 * @param genre genre of new movie to add
	 * @param year year of new movie to add
	 *
	*/
	public void insert(Movie movie)
	{
		movies[numMovies]= movie;
		numMovies++;
	}


	/**
	 * Find specified movie if it exists.
	 *
	 * @param movie the movie object we want to search for.
	 *
	 * <p>NOTE: This doesn't really work because of use of ==</p>
	*/
	public int indexOf(Movie movie)
	{
		for (int i=0; i<numMovies; i++)
			if (movies[i]==movie)
				return i;
		return -1;
	}


	/**
	 * Remove a movie from the container.
	 *
	 * @param index the array position/id
	*/
	public void remove(Movie movie)
	{
		int pos= indexOf(movie);
		if (pos>=0) {
			numMovies--;
			movies[pos]= movies[numMovies];
		}
	}
}
