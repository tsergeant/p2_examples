/**
 * Stores movie information for a single movie.
 *
 * @author  T.Sergeant
 * @version for P2
 */
import java.io.PrintStream;

public class Movie
{
	private String title;   // title of movie
	private String genre;   // genre of movie
	private int year;       // year of movie

	public Movie(String title, String genre, int year)
	{
		this.title= title;
		this.genre= genre;
		this.year= year;
	}

	public Movie() {}
	public String toString()
	{
		return String.format("%-30s %-20s %4d",title,genre,year);
	}


	/**
	 * Two movies are equal if all their attributes are equal.
	 *
	 * @param obj the object we are comparing to
	@Override
	public boolean equals(Object obj) {
	}
	 */


	/**
	 * We sum the hashcode of each attribute for which we determine
	 * equality.
	 *
	 * NOTE: Since we override .equals we have to also override .hashCode
	 * because the rule is: "if two objects are .equal then they must have
	 * the same hashCode.
	@Override
	public int hashCode() {
	}
	 */
}
