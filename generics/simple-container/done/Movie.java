/**
 * Stores movie information for a single movie.
 *
 * @author  T.Sergeant
 * @version for P2
 */
import java.io.PrintStream;

public class Movie
{
	private String title;   // title of movie
	private String genre;   // genre of movie
	private int year;       // year of movie

	public Movie(String title, String genre, int year)
	{
		this.title= title;
		this.genre= genre;
		this.year= year;
	}

	public Movie() {}
	public String toString()
	{
		return String.format("%-30s %-20s %4d",title,genre,year);
	}


	/**
	 * Getter for the year attribute.
	 *
	 * @return value of year
	 */
	public int getYear()
	{
		return year;
	}


	/**
	 * Writes title, genre, year on separate lines to the provided PrintStream.
	 *
	 * @param f the file to which we are to write
	 */
	public void writeToFile(PrintStream f)
	{
		f.printf("%s\n%s\n%d\n",title,genre,year);
	}


	/**
	 * Compares, using a case-insensitive, partial match, the title to the
	 * provided search string.
	 *
	 * @param str the provided search str
	 */
	public boolean matchesTitle(String str)
	{
		return title.toLowerCase().indexOf(str) >= 0;
	}


	/**
	 * Compares, using a case-insensitive match, the genre to the provided search
	 * string.
	 *
	 * @param str the provided search str
	 */
	public boolean matchesGenre(String str)
	{
		return genre.equalsIgnoreCase(str);
	}


	/**
	 * Two movies are equal if all their attributes are equal.
	 *
	 * @param obj the object we are comparing to
	 */
	@Override
	public boolean equals(Object obj) {
		// make sure obj is a Movie first
		if (!(obj instanceof Movie))
			return false;
		if (obj == this)
			return true;

		Movie movie= (Movie) obj; // type case obj into a movie variable
		return movie.title.equals(this.title) && movie.genre.equals(this.genre) && 
		       movie.year==this.year;
	}


	/**
	 * We sum the hashcode of each attribute for which we determine
	 * equality.
	 *
	 * NOTE: Since we override .equals we have to also override .hashCode
	 * because the rule is: "if two objects are .equal then they must have
	 * the same hashCode.
	 */
	@Override
	public int hashCode() {
		return this.title.hashCode()+this.genre.hashCode()+this.year;
	}
}
