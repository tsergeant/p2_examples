/**
 * Driver for testing code and launching program.
 *
 * @author  Terry Sergeant
 * @version for P2
 */

public class Driver
{
	public static void main(String [] args)
	{
		AnythingContainer<String> mc= new AnythingContainer<String>();
		mc.insert("Up");
		mc.insert("Down");
		mc.insert("Sideways");
		mc.remove(0);
		mc.display();
		if (mc.indexOf("Down")>=0)
			System.out.println("Found Down");

		AnythingContainer<Movie> mc2= new AnythingContainer<Movie>();
		mc2.insert(new Movie("Up","Fun",1000));
		mc2.insert(new Movie("Down","Fun",2000));
		//mc2.remove(1);
		mc2.display();
		if (mc2.indexOf(new Movie("Up","Fun",1000)) >=0)
			System.out.println("Found Movie Up");

		AnythingContainer<Integer> mc3= new AnythingContainer<Integer>();
		mc3.insert(1);
		mc3.insert(2);
		mc3.insert(3);
		mc3.insert(4);
		mc3.remove(1);
		mc3.display();
		if (mc3.indexOf(3)>=0)
			System.out.println("Found 3");


	}
}
