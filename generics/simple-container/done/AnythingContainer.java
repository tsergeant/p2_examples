/**
 * A Movie container to the support the PersonalMovieManager.
 *
 * @author Terry Sergeant
 * @version for P2
 *
*/

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.PrintStream;

public class AnythingContainer<T>
{
	public static final int MAXBOOKS= 10000;
	private String dataFile;
	T [] movies;
	int numMovies;

	public AnythingContainer()
	{
		movies= (T []) new Object[100];
		numMovies= 0;
	}

	/**
	 * Displays all movie information.
	*/
	public void display()
	{
		int i;
		System.out.println("------------------------------------------------");
		System.out.printf("%6s %-30s\n","NUM","DATA");
		for (i=0; i<numMovies; i++)
			System.out.printf("%6s %s\n",i,movies[i]);
	}


	/**
	 * Add new movie to container.
	 *
	 * @param title title of new movie to add
	 * @param genre genre of new movie to add
	 * @param year year of new movie to add
	*/
	public void insert(T value)
	{
		movies[numMovies]= value;
		numMovies++;
	}


	/**
	 * Remove a movie from the container.
	 *
	 * @param index the array position/id
	*/
	public void remove(int index)
	{
		if (index>=0 && index<numMovies) {
			movies[index]= movies[numMovies-1];
			numMovies--;
		}
	}


	/**
	 * Search container for searchElem.
	 * 
	 * @param searchElem the element we are searching for
	 * @return position of found element; -1 if not found
	*/
	public int indexOf(T searchElem)
	{
		int i;
		for (i=0; i<numMovies; i++) {
			System.out.println(movies[i]);
			if (searchElem.equals(movies[i]))
				return i;
		}
		return -1;
	}
}
