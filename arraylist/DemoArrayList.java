/**
 * Demonstrates use of Java's ArrayList.
 *
 * @author  TSergeant
 * @version for Program Design 2
 */

import java.util.ArrayList;
import java.util.ListIterator;

public class DemoArrayList
{
	public static void main(String [] args)
	{
		int pos;
		ArrayList<String> al;

		al= new ArrayList<String>();

		al.add("hi");
		al.add("there");
		al.add("how");
		al.add("are");
		al.add("you?");
		System.out.println(al.size());
		System.out.println(al.get(0));
		System.out.println(al.get(1));

		pos= al.indexOf("how");
		if (pos < 0) {
			System.out.println("'how' not found");
		}
		else {
			System.out.println("found 'how' at pos: " + pos);
		}

		System.out.println("------------------------------");
		for (int i=0; i<al.size(); i++) {
			System.out.println(al.get(i));
		}
		al.remove("are");

		System.out.println("------------------------------");
		ListIterator<String> it;
		it= al.listIterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}
}
