This directory contains a number of programs that illustrate the following concepts:

1. How to read from a file using Scanner and using BufferedReader
2. How to time code (structured programming approach)
3. How to time code (object-oriented approach)
4. The impact of the static keyword on methods and attributes.


Recommended sequence:

1) Play with CountWordsStructured.java. Especially observe the difference in
time for reading all the words of a file using Scanner vs. BufferedReader. Also
observe the mechanism/commands for timing the code.

2) Take a look at CodeTimer.java to see an example of a class that will handle
the details of timing code. Imagine how you might use this class.

3) Look at CountWordsOOP.java and see an example of how to use the CodeTimer
class. Much simpler than having to remember the name of the timing function, its
unit of measure, and its return type.

4) Look at StaticCodeTimer.java. It is the same as CodeTimer.java except that
the attributes for holding start and stop times have been made static. How
will this affect the behavior/use of the class when timing code?

5) CountWordsStatic.java is identical to CountWordsOOP.java except that
CodeTimer has been replaced by StaticCodeTimer. Observe the results. Explain the
results.

6) Read StaticDemo.java which contains an example of a static attribute that
could make sense. Observe how it works and play with the code for a while.

