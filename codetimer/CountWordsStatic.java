/**
 * Count the number of words in a file two ways and compare the time elapsed.
 *
 * @author	Terry Sergeant
 * @version for P2
 *
 * NOTE: This file uses an object-oriented programming approach of handling the
 * timing of the code. Contrast this with CountWordsStructured.java.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileInputStream;
import java.util.Scanner;

public class CountWordsStatic
{
	public static void main(String [] args) throws Exception
	{
		StaticCodeTimer timer1,timer2;  // for tracking time elapsed for the two approaches
		int wordCount1;    // number of words read from file using Scanner
		int wordCount2;    // number of words read from file using BufferedReader
		int charCount1;    // number of characters in words read from file using Scanner
		int charCount2;    // number of characters in words read from file using BufferedReader
		String word;       // word most recently read from the file


		// Read words using Scanner and count them and measure how long it takes
		timer1= new StaticCodeTimer();
		timer1.start();
		wordCount1= 0;
		charCount1= 0;
		Scanner wordfile1= new Scanner(new FileInputStream("words.txt"));
		while (wordfile1.hasNextLine()) {
			word= wordfile1.nextLine();
			charCount1+= word.length();
			wordCount1++;
		}
		wordfile1.close();
		timer1.stop();


		// Read words using BufferedReader and count them and measure how long it takes
		timer2= new StaticCodeTimer();
		timer2.start();
		wordCount2= 0;
		charCount2= 0;
		BufferedReader wordfile2= new BufferedReader(new FileReader("words.txt"));
		while ((word= wordfile2.readLine()) != null) {
			charCount2+= word.length();
			wordCount2++;
		}
		wordfile2.close();
		timer2.stop();


		// display results
		System.out.printf("Word Count (Scanner)        : %d\n",wordCount1);
		System.out.printf("Word Count (BufferedReader) : %d\n",wordCount2);
		System.out.printf("Char Count (Scanner)        : %d\n",charCount1);
		System.out.printf("Char Count (BufferedReader) : %d\n",charCount2);
		System.out.println("------------------------------------------");
		System.out.printf("Time (Scanner)              : %s seconds\n",timer1);
		System.out.printf("Time (BufferedReader)       : %s seconds\n",timer2);
	}
}
