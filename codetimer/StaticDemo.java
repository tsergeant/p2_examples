/**
 * Show behavior of static methods and attributes in an instantiated class.
 *
 * @author	Terry Sergeant
 * @version Spring 2015
 *
*/

import java.util.Scanner;

public class StaticDemo
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);

		Student a,b,c;

		a= new Student("Mary",3.3);
		b= new Student("Fred",2.8);
		c= new Student("Alice",3.7);

		System.out.println(a.getStudentCount());

		/*
		System.out.println(a.calcGrade(1.0));
		System.out.println(Student.calcGrade(2.0));
		System.out.println(Student.getStudentCount());
		*/
	}

}


class Student
{
	private String name;
	private double gpa;
	private static int studentCount;


	public Student(String name, double gpa)
	{
		this.name= name;
		this.gpa= gpa;
		studentCount++;
	}

	public static int getStudentCount() { return studentCount; }

	public void show()
	{
		System.out.println(name+" "+gpa);
	}


	public static String calcGrade(double gpa)
	{
		if (gpa >= 3.5) return "A";
		if (gpa >= 3.0) return "B";
		if (gpa >= 2.5) return "C";
		if (gpa >= 2.0) return "D";
		return "F";
	}
}
