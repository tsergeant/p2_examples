/**
 * Demonstrate a structured-programming view of a class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
 * We keep information about a student.  See StudentDemo.java
 *
*/

class Student4
{

	int id;
	String name;
	double gpa;


	public Student4(int newid, String newname, double newgpa)
	{
		id= newid;
		name= newname;
		gpa= newgpa;
	}

	public Student4()
	{
		id= 0;
		name= "Unknown";
		gpa= 0.0;
	}

	public String toString()
	{
		return "id: "+id+", name: "+name+", gpa: "+gpa;
	}
}
