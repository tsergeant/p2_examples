/**
 * Demonstrate a structured-programming view of a class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
 * <pre>
 * WARNING!  We are considering classes from a "structured programming"
 * viewpoint and NOT from an object-oriented viewpoint.  There is a lot
 * more to classes than these examples suggest!
 *
 * Lessons:
 * 1) Classes can be stored in separate source files and refered to as if
 *    they exist in the current source file.  (Note that class Student2 is
 *    now in a separate document called "Student2.java" ... furthermore,
 *    Student2.java gets automatically compiled whenever I compile
 *    StudentDemo2.java!
 *
 * 2) Objects can be passed as parameters ... and all pieces of the object
 *    get passed as a unit.
 *
 * 3) Furthermore, whenever we pass an object as a parameter, a copy is
 *    made of the reference, but, because it IS a reference, any changes
 *    made happen to the original ... JUST LIKE ARRAYS!
 *
*/

import java.util.Scanner;

public class StudentDemo2
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		Student2 mary;
		int i;

		mary= new Student2();

		mary.id= 12345;
		mary.name= "Mary Jane";
		mary.gpa= 3.6;

		showInfo(mary);
		setInfo(mary);
		showInfo(mary);


	}

	public static void showInfo(Student2 stu)
	{
		System.out.println("Id  : "+stu.id);
		System.out.println("Name: "+stu.name);
		System.out.println("GPA : "+stu.gpa);
	}


	public static void setInfo(Student2 stu)
	{
		stu.id= 54321;
		stu.name= "Sammy";
		stu.gpa= 3.59;
	}



}

