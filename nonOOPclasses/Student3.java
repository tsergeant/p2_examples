/**
 * Demonstrate a structured-programming view of a class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
 * We keep information about a student.  See StudentDemo.java
 *
*/

class Student3
{

	int id;
	String name;
	double gpa;


	public Student3(int newid, String newname, double newgpa)
	{
		id= newid;
		name= newname;
		gpa= newgpa;
	}

	public Student3()
	{
		id= 0;
		name= "Unknown";
		gpa= 0.0;
	}
}

