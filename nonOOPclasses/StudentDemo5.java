/**
 * Demonstrate a structured-programming view of a class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
 * <pre>
 * WARNING!  We are considering classes from a "structured programming"
 * viewpoint and NOT from an object-oriented viewpoint.  There is a lot
 * more to classes than these examples suggest!
 *
 * Lessons:
 * By defining a toString() method in the class you can simplify code to
 * display the contents of a class.
 * </pre>
*/

import java.util.Scanner;

public class StudentDemo5
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		Student4 [] student= new Student4[10];
		int i;
		Student4 fred;
		String fun;


		//System.out.println(fred);
		//fred= new Student4(5555,"Fred",2.2);
		//System.out.println(fred);
		/*
		fun= fred.toString();
		System.out.println(fun);
		*/


		// create/initialize 10 fictitious students
		for (i=0; i<student.length; i++)
			student[i]= new Student4(i+1000,"Student #"+i,Math.random()*2.0+2.0);


		/* Display them.  NOTE: This automatically invokes the "toString()"
		 * method
		*/
		for (i=0; i<student.length; i++)
			System.out.println("student["+i+"]= "+student[i]);
	}

}

