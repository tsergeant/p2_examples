/**
 * Demonstrate a structured-programming view of a class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
 * <pre>
 * WARNING!  We are considering classes from a "structured programming"
 * viewpoint and NOT from an object-oriented viewpoint.  There is a lot
 * more to classes than these examples suggest!
 *
 * Lessons:
 * 1) Classes typically define a constructor for the purpose of
 *    initializing the parts of the class.  A constructor is a method whose
 *    name is the same as the class.  Constructors do not have return types
 *    (not even void).  A constructor is automatically called when "new" is
 *    used.  See Student3.java.
 *
 * 2) It is possible to create multiple constructors.  Notice how fred
 *    calls a different constructor than does mary. Again, see
 *    Student3.java
 * </pre>
*/

import java.util.Scanner;

public class StudentDemo3
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		Student3 mary,fred;

		mary= new Student3(12345,"Mary Jane", 3.6); // constructor is called here
		showInfo(mary);  // NOTE that mary is already initialized

		// We are calling the constructor which has no parameters ...
		fred= new Student3();
		showInfo(fred);

	}

	public static void showInfo(Student3 stu)
	{
		System.out.println("Id  : "+stu.id);
		System.out.println("Name: "+stu.name);
		System.out.println("GPA : "+stu.gpa);
	}

}

