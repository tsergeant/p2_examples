/**
 * Demonstrate a structured-programming view of a class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
 * <pre>
 * WARNING!  We are considering classes from a "structured programming"
 * viewpoint and NOT from an object-oriented viewpoint.  There is a lot
 * more to classes than these examples suggest!
 *
 * Lessons:
 * 1) Classes can be used to combine multiple elements of possibly
 *    different types all under the name name.  Elements are distinquished
 *    from one another based on their attribute name.
 *
 * 2) Variables of a class type are references to the larger object.  We
 *    must use "new" to create the larger object.  When we use "new" we say
 *    that we have created an "instance" of the class, which we call an
 *    "object".
 *
 * 3) When we use "new" the elements inside the newly created object are
 *    automatically initialized.
 *
 * 4) We use a period (.) to delineate the attribute to which we want to
 *    refer.
 * 5) NOTE: We do NOT put the "public" qualifier in front of the second
 *    class.
*/

import java.util.Scanner;

public class StudentDemo1
{
	public static void main(String [] args)
	{
		Scanner cin= new Scanner(System.in);
		Student mary;

		mary= new Student();

		System.out.println("Reference: "+mary);
		System.out.println("Id  : "+mary.id);
		System.out.println("Name: "+mary.name);
		System.out.println("GPA : "+mary.gpa);

		mary.id= 12345;
		mary.name= "Mary Jane";
		mary.gpa= 3.6;

		System.out.println("Id  : "+mary.id);
		System.out.println("Name: "+mary.name);
		System.out.println("GPA : "+mary.gpa);

	}
}


class Student
{
	int id;
	String name;
	double gpa;
}

