/**
 * Demonstrate a structured-programming view of a class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
 * <pre>
 * WARNING!  We are considering classes from a "structured programming"
 * viewpoint and NOT from an object-oriented viewpoint.  There is a lot
 * more to classes than these examples suggest!
 *
 * Lessons:
 * 1) You can have an array of objects!!
 *
 * 2) What you get is an array of references to objects, though!  See the
 *    output of the first loop.  Those references are all set to "null"
 *    (which means that they don't refer to anything).
 *
 * 3) If we want to have 10 Student objects then we need to use "new" 10
 *    times to create each of the 10 objects.  See the second loop!
 *
 * 4) In the third loop we print the references again, but now they are not
 *    null.
 *
 * 5) To print the individual pieces we have to treat them as individuals
 *    as shown in the fourth loop.
 *
 * 6) OR we couldn't passed each reference to a method that handles
 *    printing for us.
 *
 * 7) For one more insight, checkout StudentDemo5.java (which uses
 *    Student4.java).
 * </pre>
*/

import java.util.Scanner;

public class StudentDemo4
{
	public static void main(String [] args)
	{
		Scanner kb= new Scanner(System.in);
		int i;
		Student3 [] student;

		student= new Student3[10];

		System.out.println("First loop");
		for (i=0; i<student.length; i++)
			System.out.println("student["+i+"]= "+student[i]);

		/* second loop */
		for (i=0; i<student.length; i++)
			student[i]= new Student3(i+1000,"Student #"+i,Math.random()*2.0+2.0);


		System.out.println("Third loop");
		for (i=0; i<student.length; i++)
			System.out.println("student["+i+"]= "+student[i]);

		System.out.println("Fourth loop");
		for (i=0; i<student.length; i++)
		{
			System.out.println("student["+i+"].id= "+student[i].id);
			System.out.println("student["+i+"].name= "+student[i].name);
			System.out.println("student["+i+"].gpa= "+student[i].gpa);
		}

		System.out.println("Fifth loop");
		for (i=0; i<student.length; i++)
			showInfo(student[i]);
	}



	public static void showInfo(Student3 stu)
	{
		System.out.println("Id  : "+stu.id);
		System.out.println("Name: "+stu.name);
		System.out.println("GPA : "+stu.gpa);
	}

}

