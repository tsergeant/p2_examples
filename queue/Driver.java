public class Driver {
	public static void main(String[]args) {
		MyQueue mq= new MyQueue();

		mq.insert(4);
		mq.insert(9);
		mq.insert(3);
		mq.insert(7);
		mq.insert(20);
		mq.insert(11);


		while (!mq.isEmpty()) {
			System.out.println(mq.remove());
		}
	}


	public static int getSurvivor(int numPeople, int skipSize) {
		MyQueue q= new MyQueue();
		int i;

		for (i=0; i<numPeople; i++) {
			q.insert(i);
		}
		do {
			for (i=0; i<skipSize; i++) {
				q.insert(q.remove());
			}
			q.remove();
		} while (q.size() > 1);

		return q.remove();
	}
}
