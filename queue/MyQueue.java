public class MyQueue {
	private ListNode front,back;
	private int size;

	public MyQueue() {
		front= null;
		back= null;
		size= 0;
	}

	public void insert(int val) {
		ListNode e= new ListNode(val);
		if (front==null) {
			front= e;
		}
		else {
			back.next= e;
		}
		back= e;
		size++;
	}

	public int remove() {
		int val= front.data;
		front= front.next;
		size--;
		return val;
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size==0;
	}
}
