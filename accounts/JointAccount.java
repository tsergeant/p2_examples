/**
 * A joint bank account class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
*/

import java.text.DecimalFormat;

public class JointAccount extends Account
{
	protected String jssn;   // joint owner SSN

	/**
	 * Creates new JointAccount.
	 *
	 * @param id   Account number being created.
	 * @param amt  Amount of initial deposit.
	 * @param ssn  Social security number of owner.
	 * @param jssn Social security number of joint owner.
	*/
	public JointAccount(int id, double amt, String ssn, String jssn)
	{
		super(id,amt,ssn);
		this.jssn= jssn;
	}


	/**
	 * Display account information real pretty-like.
	*/
	public void display()
	{
		System.out.println("|---------------------------------|");
		System.out.println(" Account #: "+acct_no);
		System.out.println(" Social SN: "+ssn+" and "+jssn);
		System.out.println(" Balance  : $"+(new DecimalFormat("#0.00")).format(bal));
	}
}

