/**
 * Demonstrates polymorphism with the various accounts.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

import java.util.Scanner;

public class DemoAll
{
	public static void main(String [] args)
	{
		int i;

		Account [] acc= new Account[5];
		acc[0]= new Account(1234,100.00, "111-22-3333");
		acc[1]= new JointSavingsAccount(2345,1000.00, "222-33-4444","999-88-8686",0.015);
		acc[2]= new JointAccount(4321,245.00, "333-22-1111","444-55-6666");
		acc[3]= new Account(5678,250.00, "555-66-7777");
		acc[4]= new JointSavingsAccount(8765,1245.00, "666-77-1234","666-77-1235",0.020);

		for (i=0; i<acc.length; i++)
			acc[i].display();
	}
}

