/**
 * Demonstrates joint accounts, joint savings accounts and plain accounts.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

import java.util.Scanner;

public class JointSavingsAccountDemo
{
	public static void main(String [] args)
	{
		Scanner cin= new Scanner(System.in);
		Account freds= new Account(1234,100.00, "111-22-3333");
		JointAccount marys= new JointAccount(4321,245.00, "333-22-1111","444-55-6666");
		JointSavingsAccount sams= new JointSavingsAccount(5999,1000.00, "553-22-1111","664-55-6666",0.015);

		freds.display();
		marys.display();
		sams.display();

		System.out.println("Adding interest to "+sams);
		System.out.println("Interest earned this period: "+sams.accrueInterest());
		sams.display();

		System.out.print("Transfering $500.00 from "+sams+" to "+marys+" ...");
		if (sams.transferTo(marys, 500.00))
			System.out.print("Successful!\n");
		else
			System.out.print("Failed!\n");
		sams.display();
		marys.display();

		System.out.print("Transfering $50.00 from "+freds+" to "+sams+" ...");
		if (freds.transferTo(sams,50.00))
			System.out.print("Successful!\n");
		else
			System.out.print("Failed!\n");
		freds.display();
		sams.display();

		/*
		int i;
		Account [] a= new Account[3];
		a[0]= new Account(1234,100.00, "111-22-3333");
		a[1]= new JointAccount(4321,245.00, "333-22-1111","444-55-6666");
		a[2]= new JointSavingsAccount(5999,1000.00, "553-22-1111","664-55-6666",0.035);

		for (i=0; i<3; i++)
			a[i].display();

		*/
	}
}

