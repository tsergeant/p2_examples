/**
 * A simple bank account class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
*/

import java.text.DecimalFormat;

public class Account
{
	protected int    acct_no;  // account number
	protected String ssn;      // owner ss no.
	protected double bal;      // current balance

	/**
	 * Creates new Account.
	 * @param id	 Account number being created.
	 * @param amt	Amount of initial deposit.
	 * @param ssn	 Social security number of owner.
	*/
	public Account(int id, double amt, String ssn)
	{
		acct_no= id;
		bal= amt;
		this.ssn= ssn;
	}


	/** Deposit amt into account (if amt is positive). */
	public void deposit(double amt)
	{
		if ( amt > 0 )
			bal+= amt;
	}


	/** Withdraw amt from account and return true if successful.
	 * @return true if withdrawal is successful; false otherwise
	*/
	public boolean withdraw(double amt)
	{
		if( amt <= 0 || amt > bal)
			return false;	 // failure
		bal-= amt;
		return true;
	}


	/**
	 * Transfer amt from current account to "dest".
	 * @param dest Account to which money should be moved.
	 * @param amt	Amount to be transfered.
	 * @return true if transaction succeeds; false otherwise.
	*/
	public boolean transferTo(Account dest, double amt)
	{
		boolean ok = this.withdraw(amt);
		if (ok)
			dest.deposit(amt);
		return ok;
	}


	/** Display account information real pretty-like.	*/
	public void display()
	{
		System.out.println("|---------------------------------|");
		System.out.println(" Account #: "+acct_no);
		System.out.println(" Social SN: "+ssn);
		System.out.println(" Balance  : $"+(new DecimalFormat("#0.00")).format(bal));
	}


	/** @return account number */
	public String toString()
	{
		return ""+acct_no;
	}
}
