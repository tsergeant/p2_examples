/**
 * Demonstrates using a plain old bank account.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

import java.util.Scanner;

public class AccountDemo
{
	public static void main(String [] args)
	{
		Scanner cin= new Scanner(System.in);
		Account freds, marys;
		freds= new Account(1234,100.00, "111-22-3333");
		marys= new Account(4321,245.00, "333-22-1111");
		freds.display();
		marys.display();

		marys.deposit(122.00);
		if (freds.withdraw(34.33))
			System.out.print( "Withdrawal was successful!\n");
		if (!freds.withdraw(134.77))
			System.out.print( "Attempt to withdraw failed!\n");
		freds.display();
		marys.display();

		System.out.print( "Transfering $100.00 from "+marys+" to "+freds+" ...");
		if (marys.transferTo(freds, 100.00))
			System.out.print( "Successful!\n");
		else
			System.out.print( "Failed!\n");
		freds.display();
		marys.display();
	}
}
