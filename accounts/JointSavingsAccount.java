/**
 * A joint savings account class.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
*/

public class JointSavingsAccount extends JointAccount
{
	protected double apr;    // the annual percentage rate this account earns


	/**
	 * Creates new JointAccount.
	 *
	 * @param id   Account number being created.
	 * @param amt  Amount of initial deposit.
	 * @param ssn  Social security number of owner.
	 * @param jssn Social security number of joint owner.
	*/
	public JointSavingsAccount(int id, double amt, String ssn, String jssn, double apr)
	{
		super(id,amt,ssn,jssn);
		this.apr= apr;
	}


	/**
	 * Apply interest earnings on a monthly basis.
	 *
	 * @return The amount of interest earned this period.
	*/
	public double accrueInterest()
	{
		double earned;
		if (bal > 0.0)
			earned= bal*(apr/12.0);	// monthly earnings
		else
			earned= 0.0;
		bal+= earned;
		return earned;
	}
}
