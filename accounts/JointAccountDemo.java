/**
 * Demonstrates a joint account and a plain old account.
 *
 * @author  Terry Sergeant
 * @version for Program Design 2
 *
*/

import java.util.Scanner;

public class JointAccountDemo
{
	public static void main(String [] args)
	{
		Scanner cin= new Scanner(System.in);
		Account freds= new Account(1234,100.00, "111-22-3333");
		JointAccount marys= new JointAccount(4321,245.00, "333-22-1111","444-55-6666");
		freds.display();
		marys.display();

		System.out.println("Depositing $122.00 into "+marys);
		marys.deposit(122.00);
		marys.display();
		System.out.println("Withdrawing $22.00 from "+marys);
		marys.withdraw(22.00);
		marys.display();

		System.out.print("Transfering $100.00 from "+freds+" to "+marys+" ...");
		if (freds.transferTo(marys, 100.00))
			System.out.print("Successful!\n");
		else
			System.out.print("Failed!\n");
		freds.display();
		marys.display();
	}
}

